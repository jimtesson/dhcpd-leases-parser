
use clap::Parser;
use dhcpd_leases_parser;

#[derive(Parser)]
struct Cli {
    hostname: String,
    path: std::path::PathBuf,
}

fn main() -> Result<(), &'static str>{
    let args = Cli::parse();
    let content = std::fs::read_to_string(&args.path).expect("could not read file");
    let formatted_leases = dhcpd_leases_parser::format_dhcpd_leases_file(&content);
    let result = dhcpd_leases_parser::get_last_ip_for_hostname(
        formatted_leases.as_ref().unwrap(),
        &args.hostname,
    );
    let out = match result {
        Ok(ip) => ip,
        Err(error) => {
            return Err(error.into());
        }
    };
    println!("{}", out);
    Ok(())
}
