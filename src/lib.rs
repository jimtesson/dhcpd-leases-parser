use dhcpd_parser::{
    leases::Lease,
    parser::{self, LeasesMethods},
};
use itertools::Itertools;

pub fn format_dhcpd_leases_file(_input_leases: &str) -> Result<String, &'static str> {
    // return a formatted dhcpd leases line, removing information that cannot be handled by the external lib used (dhcpd_parser)
    let formatted_line: String = _input_leases
        .split("\n")
        .filter(|line| !line.contains("#")) // remove comments
        .filter(|line| keep_line(line))
        .join("\n");

    Ok(formatted_line)
}
pub fn keep_line(line: &str) -> bool {
    // return if the line should be kept. If one of the keyword is present in the line, it is kept
    let keywords = [
        "lease",
        "}",
        "abandoned",
        "client-hostname",
        "ends",
        "hardware",
        "hostname",
        "starts",
        "uid",
    ];
    let keep = keywords.iter().any(|&s| line.contains(s));

    return keep;
}

pub fn get_last_ip_for_hostname(
    _input_leases: &str,
    _hostname: &str,
) -> Result<String, &'static str> {
    // return the last ip found for a given hostname
    let res = parser::parse(_input_leases).expect("This should be a correct lease file");

    let leases = res.leases.all();
    let mut leases_for_hostname: Vec<Lease> = leases
        .into_iter()
        .filter(|lease| {
            lease
                .client_hostname
                .as_ref()
                .is_some_and(|name| name.contains(_hostname))
        })
        .filter(|lease| lease.dates.starts.is_some())
        .map(|lease| lease)
        .collect();

    // sort by start date
    leases_for_hostname.sort_unstable_by(|a, b| {
        a.dates
            .starts
            .unwrap()
            .partial_cmp(&b.dates.starts.unwrap())
            .unwrap_or(std::cmp::Ordering::Equal)
    });

    let last_host = leases_for_hostname.last();

    match last_host {
        Some(i) => Ok(i.ip.to_string()),
        None => Err("No ip found in leases for provided hostname"),
    }
}
