#[cfg(test)]

mod tests {

    use dhcpd_leases_parser::format_dhcpd_leases_file;
    use dhcpd_leases_parser::get_last_ip_for_hostname;

    static RAW_LEASES_FILE: &'static str = "
    # The format of this file is documented in the dhcpd.leases(5) manual page.
    # This lease file was written by isc-dhcp-4.4.1
    
    # authoring-byte-order entry is generated, DO NOT DELETE
    authoring-byte-order little-endian;
    
    lease 192.168.10.13 {
        starts 2 2023/07/18 15:10:38;
        ends 2 2023/07/18 15:20:38;
        tstp 2 2023/07/18 15:20:38;
        cltt 2 2023/07/18 15:10:38;
        binding state free;
        hardware ethernet 14:42:fc:c2:32:cb;
      }
      lease 192.168.10.18 {
        starts 2 2023/07/25 15:08:05;
        ends 2 2023/07/25 15:10:05;
        tstp 2 2023/07/25 15:10:05;
        cltt 2 2023/07/25 15:08:05;
        binding state free;
        hardware ethernet 6c:30:2a:f9:4d:19;
      }
      lease 192.168.10.19 {
        starts 2 2023/07/25 15:08:06;
        ends 2 2023/07/25 15:10:06;
        tstp 2 2023/07/25 15:10:06;
        cltt 2 2023/07/25 15:08:06;
        binding state free;
        hardware ethernet 6c:30:2a:f9:4d:19;
        set vendor-class-identifier = \"AM335x UBoot SPL\";
      }
      lease 192.168.10.10 {
        starts 3 2022/09/06 08:44:05;
        ends 3 2022/09/06 08:54:05;
        cltt 3 2023/09/06 08:34:05;
        binding state active;
        next binding state free;
        rewind binding state free;
        hardware ethernet 6c:30:2a:e1:be:ee;
        client-hostname \"myhost\";
      }
      lease 192.168.10.30 {
        starts 3 2023/09/06 08:39:05;
        ends 3 2023/09/06 08:49:05;
        cltt 3 2023/09/06 08:39:05;
        binding state active;
        next binding state free;
        rewind binding state free;
        hardware ethernet 6c:30:2a:e1:be:ee;
        client-hostname \"myhost\";
      }
      lease 192.168.10.20 {
        starts 3 2022/09/06 08:44:05;
        ends 3 2022/09/06 08:54:05;
        cltt 3 2023/09/06 08:44:05;
        binding state active;
        next binding state free;
        rewind binding state free;
        hardware ethernet 6c:30:2a:e1:be:ee;
        client-hostname \"myhost\";
      }
        lease 192.168.10.19 {
        starts 2 2023/07/25 15:08:06;
        ends 2 2023/07/25 15:10:06;
        tstp 2 2023/07/25 15:10:06;
        cltt 2 2023/07/25 15:08:06;
        binding state free;
        hardware ethernet 6c:30:2a:f9:4d:19;
        set vendor-class-identifier = \"AM335x UBoot SPL\";
      }
    ";

    #[test]
    fn test_format_file_string() -> Result<(), String> {
        let formatted_leases = format_dhcpd_leases_file(RAW_LEASES_FILE);
        assert_eq!(formatted_leases.as_ref().unwrap().contains("#"), false);
        assert_eq!(formatted_leases.as_ref().unwrap().contains("client-hostname"), true);
        assert_eq!(
            formatted_leases.as_ref().unwrap().contains("authoring"),
            false
        );

        Ok(())
    }

    #[test]
    fn test_find_hostname() -> Result<(), String> {

        let formatted_leases = format_dhcpd_leases_file(RAW_LEASES_FILE);
        let target_host = "myhost";
        let ip = get_last_ip_for_hostname(formatted_leases.as_ref().unwrap(), target_host);
        assert_eq!(ip?, "192.168.10.30".to_string());

        Ok(())
    }
    #[test]
    fn test_no_hostname() -> Result<(), String> {

        let formatted_leases = format_dhcpd_leases_file(RAW_LEASES_FILE);
        let target_host = "pipouette";
        let ip = get_last_ip_for_hostname(formatted_leases.as_ref().unwrap(), target_host);
        assert_eq!(ip.is_err(), true);

        Ok(())
    }
}
