# Dhcpd-leases-parser
Simple parser returning the last ip address attributed by isc-dhcpd-server, for a given host-name. 

Usage: `dhcpd-leases-parser <HOSTNAME> <PATH> `

## Build

`cargo build`

## Test

`cargo test `

## Usage example

` ./dhcpd-leases-parser my-debian-vm /var/lib/dhcp/dhcpd.leases`

`> 192.168.10.15`
